# README #

Reads the datasource connection info from PostgreSQL remote db.

Connects to datasource and reads the database schema from Oracle, MySQL, MSSQL, PostgreSQL using SchemaSpy.

Schema of datasource is then saved in an .XML file which is then used by ETL.

