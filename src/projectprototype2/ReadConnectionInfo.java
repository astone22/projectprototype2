package projectprototype2;
import java.sql.*;
import javax.swing.JOptionPane;
import java.io.*;

public class ReadConnectionInfo {
	//This file reads the information from the chin_remote_sources table for future use in creating connection to the source data base
    private Connection connection;
    public ReadConnectionInfo(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(PostgresCredentials.getDatabase(), PostgresCredentials.getUsername(), PostgresCredentials.getPassword());
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM chin_remote_data_source;"); //gets a result set of the contentchin_remote_data_source
            resultSet.next();
            SourceDataBaseInfo.setDataBaseType(resultSet.getString("databasetype"));
            EstablishConnection establishConnection;
            if(!(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgresql") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgres") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("access")  ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("msaccess") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mysql") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("oracle") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("oracledb") ||
                 SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mssql"))){
                    throw new IOException("Unknown database type");
            }
            SourceDataBaseInfo.setDatabaseName(resultSet.getString("databasename"));
            SourceDataBaseInfo.setUserName(resultSet.getString("username"));
            SourceDataBaseInfo.setPassword(resultSet.getString("password"));
            SourceDataBaseInfo.setSchema(resultSet.getString("schema"));
            SourceDataBaseInfo.setHostname(resultSet.getString("hostname"));
            try{
            SourceDataBaseInfo.setPersonTable(resultSet.getString("persontable").trim());
            }
            catch(Exception e){
            	 SourceDataBaseInfo.setPersonTable("");
            }
            try{
            SourceDataBaseInfo.setPort(resultSet.getString("port").trim());
            }
            catch(Exception e){
            	 SourceDataBaseInfo.setPort("");
            }
            connection.close();
            establishConnection = new EstablishConnection();
            
            
        }
        catch(ClassNotFoundException e){
            System.out.println("ClassNotFoundException");
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, "ClassNotFoundException", "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        catch(SQLException e){
            System.out.println("SQLException");
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, "SQLException", "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, e.getMessage(), "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
