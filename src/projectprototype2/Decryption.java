package projectprototype2;

/**
 *
 * @author dsiaukovich
 */
import java.io.File;
import java.io.PrintWriter;
import java.security.Security;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.util.*;

/**
 *
 * @author dsiaukovich
 */
public class Decryption {
	//Decryption class used for password decryption using Blowfish
    public static String decrypt(String pass){
        String decryptedPassword = "";
        try {
            Scanner myScanner = new Scanner(new File("webapps/Pathr/WEB-INF/files/key.txt"));
            
            String Key = myScanner.nextLine();
            myScanner.close();
            byte[] KeyData = Key.getBytes();
            SecretKeySpec KS = new SecretKeySpec(KeyData, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, KS);
            String inputText = pass;
            //byte[] input = inputText.getBytes();
            byte [] input = new byte[inputText.length()];
            for(int i = 0; i < input.length; i++){
                input[i] = (byte)inputText.charAt(i);
            }
            byte[] decrypted = cipher.doFinal(input);
           
            decryptedPassword = new String(decrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return decryptedPassword;
    }
}