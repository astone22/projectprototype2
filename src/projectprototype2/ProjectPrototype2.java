package projectprototype2;
import java.nio.file.Paths;
import java.io.File;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;


/**
 * @author Dzmitry Siaukovich
 */
public class ProjectPrototype2 {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(ProjectPrototype2.class);
	//This project is used to create the the schema in an XML file for migration into Postgres
    public static void main(String[] args) {
    	
    	logger.info("EVENT: ProjectPrototype Started");
    	System.out.println(Paths.get("").toAbsolutePath().toString());
        if (args.length > 0) {//Defines arguements for text line
            VisualErrors.disable();
            FieldNamesToCmd.disable();
            for (int i = 0; i < args.length; i++) {
                if (args[i].equalsIgnoreCase("visualerrors")) {
                    VisualErrors.enable();
                } else if (args[i].equalsIgnoreCase("columns")) {
                    FieldNamesToCmd.enable();
                } else if (args[i].equalsIgnoreCase("help")) {
                    System.out.println("columns\t\tshow columns processed (works only when run using \"java -jar\"");
                    System.out.println("help\t\tshow this help menu");
                    System.out.println("visualerrors\tshow errors in the popup window");
                    System.exit(0);
                } else if (args[i].equalsIgnoreCase("about")) {
                    //AboutScreen.showScreen("for Dolume, LLC");
                    System.exit(0);
                }
            }
        }

        MainFrame mainFrame = new MainFrame();
        String serverURL = "";
        Properties prop = new Properties();
        try {
			//calls the three Scriptella XMLs that will migrate the schema to Postgres, HSQLdb and eventually copy the source data to HSQLdb
//            EtlExecutor.newExecutor(new File("etl.xml")).execute();//Copies the XML schema to postgres
//            EtlExecutor.newExecutor(new File("etl1.xml")).execute();//Replicates the source data base schema in HSQLdb from Postgres
//            EtlExecutor.newExecutor(new File("etl2.xml")).execute();//Migrates the data from the source database to the newly created HSQLdb
        
        //new approach: create a Jersey connection and send the create statement to Jersey Server in Main...
//        	prop.load(new FileInputStream("server.properties"));
//			serverURL = prop.getProperty("url");
        	
//        	ClientConfig config = new DefaultClientConfig();
//    		Client client = Client.create(config);
//			WebResource service = client.resource(serverURL); //creating the connection and pass it to the etl process
//			ClientResponse response = null; //this is optional, but we can use this to get the response from WebResource 
			
//        	
//        	Etl etl = new Etl();
//			Etl1 etl1 = new Etl1(service);
//			Etl2 etl2 = new Etl2(service);
        
			prop = null;
			serverURL = null;
        }
//        catch (IOException ex) {
//			System.out.println("File server.properties not found");
//			ex.printStackTrace();
//			System.exit(1);
//		}
        catch(Exception e){
        	e.printStackTrace();
        	
        }
//        try{
//        Command c = new Command();
//        }
//        catch(IOException e){
//            
//        }
//        catch(InterruptedException e){
//            
//        }
    }
}

//class Command {
//
//    public Command() throws java.io.IOException, InterruptedException {
//
//        Process p = Runtime.getRuntime().exec("cmd /c start /wait command.cmd");
//
//        p.waitFor();
//
//        p = Runtime.getRuntime().exec("cmd /c start /wait scriptella c:\\job\\chindataHSQLDB\\etl1.xml");
//        p.waitFor();
//        //p = Runtime.getRuntime().exec("cmd /c start /wait scriptella etl2.xml");
//        //p.waitFor();
//
//    }
//}
