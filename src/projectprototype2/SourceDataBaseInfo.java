package projectprototype2;

public class SourceDataBaseInfo {
	//Static methods for storing and retrieving schema, datastore name, username, and password
    private static String sDatabaseName, sUserName, sPassword, sDataBaseType, sSchema, sPersonTable, sPort, sHostname;
    
    public static String getHostname() {
		return sHostname;
	}

	public static void setHostname(String sHostname) {
		SourceDataBaseInfo.sHostname = sHostname;
	}

	public static String getPort() {
		return sPort;
	}

	public static void setPort(String sPort) {
		SourceDataBaseInfo.sPort = sPort;
	}

	public static String getPersonTable() {
		return sPersonTable;
	}

	public static void setPersonTable(String sPersonTable) {
		SourceDataBaseInfo.sPersonTable = sPersonTable;
	}

	public static String getSchema() {
        return sSchema;
    }

    public static void setSchema(String sSchema) {
        SourceDataBaseInfo.sSchema = sSchema;
    }

    public static String getDatabaseName() {
        return sDatabaseName;
    }

    public static void setDatabaseName(String sDatabaseName) {
        SourceDataBaseInfo.sDatabaseName = sDatabaseName;
    }

    public static String getUserName() {
        return sUserName;
    }

    public static void setUserName(String sUserName) {
        SourceDataBaseInfo.sUserName = sUserName;
    }

    public static String getPassword() {
        return sPassword;
    }

    public static void setPassword(String sPassword) {
        SourceDataBaseInfo.sPassword = sPassword;
    }

    public static String getDataBaseType() {
        return sDataBaseType;
    }

    public static void setDataBaseType(String sDataBaseType) {
        SourceDataBaseInfo.sDataBaseType = sDataBaseType;
    }
    
}
