package projectprototype2;
import java.sql.*;

import javax.swing.JOptionPane;

public class EstablishConnection {
	//Establishes the connection to the source database using previously obtained information
    private Connection connection;
    public EstablishConnection(){
        try{        	
//        	Class.forName("net.sourceforge.jtds.jdbc.Driver"); //MSSQL JDBC driver
//        	Class.forName("com.mysql.jdbc.Driver"); //MYSQL JDBC driver
//        	Class.forName("org.postgresql.Driver"); //POSTGRESQL
//        	Class.forName("oracle.jdbc.driver.OracleDriver"); //ORACLE
            if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mssql")){
            	Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance(); //MSSQL JDBC driver
            	if(SourceDataBaseInfo.getPort()=="" || SourceDataBaseInfo.getPort().isEmpty()){ //SQLEXPRESS will use a different connection URL compare to the regular MSSQL
            		
            	}
            	else{
            		
            	}
                SourceDataBaseInfo.setDatabaseName("jdbc:jtds:sqlserver://"+SourceDataBaseInfo.getHostname()+":"+SourceDataBaseInfo.getPort()+"/" + SourceDataBaseInfo.getDatabaseName());
//            	SourceDataBaseInfo.setDatabaseName("jdbc:jtds:sqlserver://localhost;DatabaseName="+SourceDataBaseInfo.getDatabaseName());
            	// might need to add this at the end of the url: ";instance=SQLEXPRESS"
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("oracle")){
            	Class.forName("oracle.jdbc.driver.OracleDriver"); //ORACLE
                SourceDataBaseInfo.setDatabaseName("jdbc:oracle:thin:@"+SourceDataBaseInfo.getHostname()+":"+SourceDataBaseInfo.getPort()+":" + SourceDataBaseInfo.getDatabaseName());
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mysql")){
            	Class.forName("com.mysql.jdbc.Driver"); //MYSQL JDBC driver
            	SourceDataBaseInfo.setDatabaseName("jdbc:mysql://"+SourceDataBaseInfo.getHostname()+":"+SourceDataBaseInfo.getPort()+"/" + SourceDataBaseInfo.getDatabaseName());
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgresql")||SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgres")){
             	Class.forName("org.postgresql.Driver"); //POSTGRESQL
           	 	SourceDataBaseInfo.setDatabaseName("jdbc:postgresql://"+SourceDataBaseInfo.getHostname()+":"+SourceDataBaseInfo.getPort()+"/" + SourceDataBaseInfo.getDatabaseName());
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("access")){ //cant find the JDBC driver for access, use ODBC for now
             	Class.forName("sun.jdbc.odbc.JdbcOdbcDriver"); //ACCESS
           	 	SourceDataBaseInfo.setDatabaseName("jdbc:odbc:" + SourceDataBaseInfo.getDatabaseName());
            }
        	
        	
        	/*
        	 * old code to connect database with ODBC connection...
            String driver = "sun.jdbc.odbc.JdbcOdbcDriver";
            Class.forName(driver);
            if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mssql")){
                SourceDataBaseInfo.setDatabaseName("jdbc:odbc:" + SourceDataBaseInfo.getDatabaseName() + ";MARS_Connection=yes");
            }
            else
                SourceDataBaseInfo.setDatabaseName("jdbc:odbc:" + SourceDataBaseInfo.getDatabaseName());
//            System.out.println(SourceDataBaseInfo.getDataBaseType() + " " + SourceDataBaseInfo.getDatabaseName() + " " + SourceDataBaseInfo.getPassword() + " " + SourceDataBaseInfo.getSchema() + " " + SourceDataBaseInfo.getUserName());
            connection = DriverManager.getConnection(SourceDataBaseInfo.getDatabaseName(), SourceDataBaseInfo.getUserName(),
                            Decryption.decrypt(SourceDataBaseInfo.getPassword()));
            
            */
            String a = SourceDataBaseInfo.getDatabaseName() + " " + SourceDataBaseInfo.getUserName() + " " + SourceDataBaseInfo.getPassword() + " ";
            System.out.println(a);
            connection = DriverManager.getConnection(SourceDataBaseInfo.getDatabaseName(), SourceDataBaseInfo.getUserName(), SourceDataBaseInfo.getPassword());
            
            //RetrieveInfo retrieveInfo = new RetrieveInfo(connection);
            CreateXML createXML = new CreateXML(connection);
            
            connection.close();
        }
        catch(SQLException e){
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, e.getMessage(), "Exception", JOptionPane.ERROR_MESSAGE);
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
        catch(ClassNotFoundException e){
            System.out.println("Could not locate ODBC-JDBC driver");
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, "Could not locate ODBC-JDBC driver", "Exception", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        } catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
