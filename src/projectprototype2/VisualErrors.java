package projectprototype2;

public class VisualErrors {
	//Static methods for enabling visual errors if selected as an arguement
    private static boolean visualErrorsEnabled = false;
    public static void enable(){
        visualErrorsEnabled = true;
    }
    public static void disable(){
        visualErrorsEnabled = false;
    }
    public static boolean areEnabled(){
        return visualErrorsEnabled;
    }
}
