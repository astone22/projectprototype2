package projectprototype2;
import javax.swing.JOptionPane;
import java.util.*;
import java.io.*;
public class MainFrame{
//Used to retrieve the connection information from the data.properties file in the install directory
    public MainFrame(){
        Properties prop = new Properties();
        
        try{
            prop.load(new FileInputStream("webapps/Pathr/WEB-INF/files/data.properties"));
            PostgresCredentials.setDatabase(prop.getProperty("location"));
            PostgresCredentials.setUsername(prop.getProperty("user"));
            PostgresCredentials.setPassword(prop.getProperty("password"));
        }
        catch(IOException ex){
            System.out.println("File data.properties not found");
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, "File data.properties not found", "File not found", JOptionPane.INFORMATION_MESSAGE);
            ex.printStackTrace();
            System.exit(1);
        }
        ReadConnectionInfo readInfo = new ReadConnectionInfo();
    }
}
