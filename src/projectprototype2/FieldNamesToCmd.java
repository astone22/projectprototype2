package projectprototype2;

public class FieldNamesToCmd {
	//Static methods for outputting field names to command line
    private static boolean enabled = false;
    public static void enable(){
        FieldNamesToCmd.enabled = true;
    }
    public static void disable(){
        FieldNamesToCmd.enabled = false;
    }
    public static boolean isEnabled(){
        return enabled;
    }
}
