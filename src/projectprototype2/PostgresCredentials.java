package projectprototype2;

public class PostgresCredentials {
	//Static methods for storing/retrieving information out of Postgres
    private static String sDatabase, sUsername, sPassword;

    public static String getUsername() {
        return sUsername;
    }

    public static String getDatabase() {
		return sDatabase;
	}

	public static void setDatabase(String sDatabase) {
		PostgresCredentials.sDatabase = sDatabase;
	}

	public static void setUsername(String sUsername) {
        PostgresCredentials.sUsername = sUsername;
    }

    public static String getPassword() {
        return sPassword;
    }

    public static void setPassword(String sPassword) {
        PostgresCredentials.sPassword = sPassword;
    }
    
}
