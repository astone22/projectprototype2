package projectprototype2;
import java.sql.*;
import javax.swing.JOptionPane;
public class RetrieveInfo {
	//File for retrieving table names from source database to be written into XML
    public RetrieveInfo(Connection connection){
        try{
            ResultSet res = null;
            if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgres") ||
               SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("postgresql") ||
               SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mysql")){
                    Statement statement = connection.createStatement();
//                  String query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + SourceDataBaseInfo.getSchema() + "' AND TABLE_TYPE='VIEW'"; //uncomment this to do the testing when all all the views instead of tables...
                    res = statement.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + SourceDataBaseInfo.getSchema() + "' AND TABLE_TYPE = 'BASE TABLE'");
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("oracle") ||
                    SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("oracledb")){
                    Statement statement = connection.createStatement();
                    res = statement.executeQuery("SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER = '" + SourceDataBaseInfo.getUserName() + "' AND TABLE_NAME NOT LIKE '%$%';");
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("mssql")){
                    Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//                    String query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + SourceDataBaseInfo.getSchema() + "' AND TABLE_TYPE='VIEW'"; //uncomment this to do the testing when you want to get all the views instead of tables...
                    String query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + SourceDataBaseInfo.getSchema() + "' AND TABLE_TYPE = 'BASE TABLE'";

                    res = statement.executeQuery(query);
            }
            else if(SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("access") ||
                    SourceDataBaseInfo.getDataBaseType().equalsIgnoreCase("msaccess")){
                DatabaseMetaData meta = connection.getMetaData();
                res = meta.getColumns(null, null, null, null);
            }
            CreateXML createXML = new CreateXML(connection);
        }
        catch(SQLException e){
            if(VisualErrors.areEnabled())
                JOptionPane.showMessageDialog(null, "Error retrieving information", "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }
}
