package projectprototype2;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.sql.*;
import java.util.List;
import java.util.Properties;

import javax.swing.JOptionPane;

import ch.qos.logback.classic.Logger;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;





import java.util.ArrayList;

/**
 * 
 * @author Alan Stone
 *
 */


public class CreateXML {
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CreateXML.class);
	//This file uses the table names generated in RetrieveInfo.java to create the XML of the schema for each table
    public CreateXML(Connection connection){
    	
    	
		System.out.println("------START OF CREATEXML.JAVA-------");
    	
    	//System.out.println(SourceDataBaseInfo.getDatabaseName());
    	
		String dsnName = SourceDataBaseInfo.getDatabaseName();
		String[] dbName = dsnName.split("\\/");
		String dbType = SourceDataBaseInfo.getDataBaseType();
		String dbHostName = SourceDataBaseInfo.getHostname();
		String dbPassword = SourceDataBaseInfo.getPassword();
		String dbPort = SourceDataBaseInfo.getPort();
		String dbSchema = SourceDataBaseInfo.getSchema();
		String dbUser = SourceDataBaseInfo.getUserName();
		String dbDriver = "";
		String cmd = "";
		String xmlResult = "";
		String xmlResultNoExtension = "";
		System.out.println("Reading remote db type from chin_remote_data_source: " + dbType);
		
		
////		Properties propSchemaProperties = new Properties();
//		try {
//			FileOutputStream createSchemaProperties = new FileOutputStream("tableSchema.properties");
//			try {
//				createSchemaProperties.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		} catch (FileNotFoundException e2) {
//			e2.printStackTrace();
//		}
//		
		String schemaoutput = "";
		Properties propSchema = new Properties();
		try{
            propSchema.load(new FileInputStream("webapps/Pathr/WEB-INF/files/data.properties"));
            schemaoutput = (propSchema.getProperty("schemaoutput"));
        }
        catch(IOException ex){
            System.out.println("File data.properties not found");
            logger.info("EVENT: FILE NOT FOUND: data.properties");
        }
		System.out.println("Output Folder: " + schemaoutput);
		
		
		Properties prop = new Properties();
		String filename = "";
		try {
			FileInputStream in = new FileInputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties");
			prop.load(in);
			in.close();
		} catch (IOException e ) {
			System.out.println("tableSchema.properties not found");
			e.printStackTrace();
		}
		
    	
    	try {
    		if (dbType.equalsIgnoreCase("mysql")) {
//    			dbDriver = "\".\\lib\\mysql-connector-java-5.0.8-bin.jar\"";
    			dbDriver = "\"webapps/Pathr/WEB-INF/files/lib/mysql-connector-java-5.0.8-bin.jar\"";
    			cmd = "java -jar \"webapps/Pathr/WEB-INF/files/schemaSpy_5.0.0.jar\" -dp " + dbDriver + " -t mysql -db " + dbSchema + " -host " + dbHostName + ":" + dbPort + " -u " + dbUser + " -p " + dbPassword + " -o " + schemaoutput + " -noimplied  -nohtml";
    			xmlResult = dbSchema + ".xml";
    			xmlResultNoExtension = dbSchema;
    			prop.setProperty("filename", xmlResultNoExtension + "_scrubbed.xml");
    			FileOutputStream out = new FileOutputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties");
    			prop.save(out, null);
    			out.close();
    		}
    		else if (dbType.equalsIgnoreCase("postgres")) {
//    			dbDriver = "\".\\lib\\postgresql-9.3-1102.jdbc41.jar\"";
    			dbDriver = "\"webapps/Pathr/WEB-INF/files/lib/postgresql-9.3-1102.jdbc41.jar\"";
    			cmd = "java -jar \"webapps/Pathr/WEB-INF/files/schemaSpy_5.0.0.jar\" -dp " + dbDriver + " -t pgsql -db " + dbName[3] + " -host " + dbHostName + ":" + dbPort + " -s " + dbSchema + " -u " + dbUser + " -p " + dbPassword + " -o " + schemaoutput + " -noimplied -nohtml";
    			xmlResult = dbName[3] + "." + dbSchema + ".xml";
    			xmlResultNoExtension = dbName[3] + "." + dbSchema;
				prop.setProperty("filename", xmlResultNoExtension + "_scrubbed.xml");
    			FileOutputStream out = new FileOutputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties");
    			prop.save(out, null);
    			out.close();
    		}
    		else if (dbType.equalsIgnoreCase("mssql")) {
//				dbDriver = "\".\\lib\\jtds-1.3.1.jar\"";
    			dbDriver = "\"webapps/Pathr/WEB-INF/files/lib/jtds-1.3.1.jar\"";
				cmd = "java -jar \"webapps/Pathr/WEB-INF/files/schemaSpy_5.0.0.jar\" -dp " + dbDriver + " -t mssql-jtds -db " + dbName[3] + " -host " + dbHostName + " -port " + dbPort + " -s dbo -u " + dbUser + " -p " + dbPassword + " -o " + schemaoutput + " -noimplied -nohtml";
    			xmlResult = dbName[3] + "." + dbSchema + ".xml";
    			xmlResultNoExtension = dbName[3] + "." + dbSchema;
				prop.setProperty("filename", xmlResultNoExtension + "_scrubbed.xml");
    			FileOutputStream out = new FileOutputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties");
    			prop.save(out, null);
    			out.close();
    		}
    		else if (dbType.equalsIgnoreCase("oracle")) {
    			dbName = dsnName.split(":");
    			dbDriver = "\"webapps/Pathr/WEB-INF/files/lib/ojdbc6.jar\"";
    			cmd = "java -jar \"webapps/Pathr/WEB-INF/files/schemaSpy_5.0.0.jar\" -dp " + dbDriver + " -t orathin -db " + dbName[5] + " -host " + dbHostName + " -port " + dbPort + " -s " + dbSchema + " -u " + dbUser + " -p " + dbPassword + " -o " + schemaoutput + " -noimplied -nohtml";
    			xmlResult = dbName[5] + "." + dbSchema + ".xml";
    			xmlResultNoExtension = dbName[5] + "." + dbSchema;
				prop.setProperty("filename", xmlResultNoExtension + "_scrubbed.xml");
    			FileOutputStream out = new FileOutputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties");
    			prop.save(out, null);
    			out.close();
    		}
    		System.out.println("Command to run SchemaSpy Jar for " + dbType + ": " + cmd);
    		Process process = Runtime.getRuntime().exec(cmd);
    		if(process.waitFor() != 0){
    			Thread.sleep(500);
    		}
    		//Thread.sleep(2000);
			System.out.println("SchemaSpy: " + dbType + " schema xml file created: " + xmlResult);
			
    		
    		//PUT XML FILE INTO 'file' and 'fileScrubbed' OBJECT FOR SCRUBBING so the xml file can be read correctly without
			// multiple rows of the same row in metadatafields
    		String xmlResultFinal = schemaoutput + "\\" + xmlResult;
    		String xmlResultNoExtensionFinal = schemaoutput + "\\" + xmlResultNoExtension + "_scrubbed.xml";
    		File file = null;
    		File fileScrubbed = null;
    		file = new File(xmlResultFinal);
    		fileScrubbed = new File(xmlResultNoExtensionFinal);
    		replaceSelected(file, fileScrubbed, "            <column ascending");
//    		deleteXMLNode(file, fileScrubbed);
    		System.out.println("file: " + file);
    		System.out.println("fileScrubbed: " + fileScrubbed);
    		
    		System.out.println("xmlResultFinal: " + xmlResultFinal);
    		System.out.println("xmlResultNoExtensionFinal: " + xmlResultNoExtensionFinal);
    		
    		System.out.println("tableSchema.properties filename field set to " + prop.getProperty("filename"));
    		System.out.println("------END OF CREATEXML.JAVA-------");
    		
    		
		} catch (IOException e1) {
			System.out.println("IOException error.");
			e1.printStackTrace();
		}
    	catch (InterruptedException e) {
    		e.printStackTrace();
    	}
    }
    
    
    
	public static void replaceSelected(File file, File fileScrubbed, String type) throws IOException {

		
		System.out.println("String searched for: " + type);
	    // we need to store all the lines
	    List<String> lines = new ArrayList<String>();

	    // first, read the file and store the changes
	    BufferedReader in2 = new BufferedReader(new FileReader(file));
	    String line = in2.readLine();
	    while (line != null) {
	        if (line.startsWith(type)) {
	            line = "";
	        }
	        lines.add(line);
	        line = in2.readLine();
	    }
	    in2.close();

	    // now, write the file again with the changes
	    PrintWriter out = new PrintWriter(fileScrubbed);
	    for (String l : lines)
	        out.println(l);
	    out.close();
	    System.out.println("XML file successfully scrubbed.");

	}
	
//	public void deleteXMLNode(File file, File fileScrubbed){
//		try 
//        {  
//		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
//        DocumentBuilder builder = factory.newDocumentBuilder();  
//        Document doc = builder.parse(file); 
//        NodeList nodeList = doc.getElementsByTagName("index");
//        System.out.println(nodeList.getLength());
//        for(int i=0; i<nodeList.getLength(); i++){
//        	Node tmp = nodeList.item(i);
//        	tmp.getParentNode().removeChild(tmp);
//        }
//        FileOutputStream fos = new FileOutputStream(fileScrubbed);
//		
//		TransformerFactory transformerFactory = TransformerFactory.newInstance();
//		Transformer transformer = transformerFactory.newTransformer();
//		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
//		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
//		DOMSource source = new DOMSource(doc);
//		// Output to console for testing
//		StreamResult result = new StreamResult(fos);
//		
//		//need to find a way to convert the XML to String
////		StreamResult result = new StreamResult(new File("C:\\file.xml"));
//		
//		transformer.transform(source, result);
//		fos.close();
//        
//        } catch (Exception e) {  
//            e.printStackTrace();  
//        } 
//	}

}
